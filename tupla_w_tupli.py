krotka1 = (3, 666, 5)
krotka2 = (1, 2, 3, 4, 5, 6, 7, 8)

def check(krt1, krt2):
    result = True
    for e in krt1:
        if e not in krt2:
            result = False

    return result

def do_sklad(krt1, krt2):
    nowa_lista = []
    for x in krt1:
        nowa_lista.append(x in krt2)
    return nowa_lista

def skladana(krt1, krt2):
    return [x in krt2 for x in krt1]

#print(all(skladana(krotka1, krotka2)))
#print(any(skladana(krotka1, krotka2)))

def roz_adama(krt1, krt2):
    return set(krt1).issubset(krt2)

print(roz_adama(krotka1, krotka2))
