krotka2 = (1, 2, 3, 4, 5, 6, 7, 8)

def above5(krotka2):
    new = []
    for e in krotka2:
        if e > 5:
            new.append(e)
    return new

def above5_skladny(krotka2):
    return [e for e in krotka2 if e > 5]

print(above5(krotka2))
print(above5_skladny(krotka2))
