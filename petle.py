x = 0

# while x < 10:
#     print(x)
#     x += 1

# for x in range(10):
#     print(x)

# for x in [1 ,2 ,3]:
#     print(x)

# for x in (1 , 1, 1):
#     print(x)

slownik = {32 : 'adam', 4 : 'alan', 'aaaa' : 'kotek'}

# for x in slownik:
#     print(x)

# for x in slownik.keys():
#     print(x)

# for x in slownik.values():
#     print(x)

# for x in slownik.keys():
#     print(slownik[x])

# for key, value in slownik.items():
#     print(f'key = {key}')
#     print(f'value = {value}')

lista = ['adam', 'alan', 'kotek']

# counter = 0
# for x in lista:
#     print(x)
#     print(counter)
#     counter += 1

# for counter, x in enumerate(lista):
#     print(x)
#     print(counter)
