x = 5

if x == 10:
    print('1')
elif x == 6:
    print('2')
else:
    print('3')

def nic():
    x = 10
    return True if x > 5 else False

print(nic())