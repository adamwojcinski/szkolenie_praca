tekst = 'adam jest fajny'

def fuu(tekst):
    res = ''
    for i in tekst.split():
        res += i[::-1] + ' '
    return res

def for_jest_fajny(tekst):
    nowa_lista_odwroconych_slow = []
    for slowo in tekst.split():
        nowa_lista_odwroconych_slow.append(slowo[::-1])
    return ' '.join(nowa_lista_odwroconych_slow)

def lista_skladna_jest_fajniejsza(tekst):
    return ' '.join(slowo[::-1] for slowo in tekst.split())

print(lista_skladna_jest_fajniejsza(tekst))
print(for_jest_fajny(tekst))
#print(fuu(tekst))
