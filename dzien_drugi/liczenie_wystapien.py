lista1 = [1, 1, 2, 1, 4, 4, 5]

def funkcja(lista):
    slownik = {}
    for item in lista:
        if item in slownik:
            slownik[item] += 1
        else:
            slownik[item] = 1
    return slownik

#------------------------------------

List = [1,1,2,1,4,4,5]
def count(l):
    results = {}
    for e in set(l):
        results[e] = l.count(e)
    return results

def count_skladny(l):
    return {e : l.count(e) for e in set(l)}

#print(count_skladny(List))

from collections import Counter

z = dict(Counter(List))
print(z)